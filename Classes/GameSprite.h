//
//  GameSprite.h
//  AirKockey
//
//  Created by 최崔진국 on 8/13/15.
//
//

#ifndef __AirKockey__GameSprite__
#define __AirKockey__GameSprite__

#include <stdio.h>
#include "cocos2d.h"

using namespace cocos2d;

class GameSprite : public Sprite{

public:
    CC_SYNTHESIZE(Vec2, _nextPosition, NextPosition);
    CC_SYNTHESIZE(Vec2, _vector, Vector);
    CC_SYNTHESIZE(Touch*, _touch, Touch);
    GameSprite();
    virtual ~GameSprite();
    static GameSprite * gameSpriteWithFile(const char* pszFileName);
    virtual void setPosition(const Vec2& pos) override;
    float radius();
};


#endif /* defined(__AirKockey__GameSprite__) */
