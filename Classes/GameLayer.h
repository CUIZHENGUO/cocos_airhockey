//
//  GameLayer.h
//  AirKockey
//
//  Created by 최崔진국 on 8/13/15.
//
//

#ifndef __AirKockey__GameLayer__
#define __AirKockey__GameLayer__

#define GOAL_WIDTH 400

#include <stdio.h>
#include "cocos2d.h"
#include "GameSprite.h"
#include "SimpleAudioEngine.h"

using namespace cocos2d;
using namespace CocosDenshion;

class GameLayer : public Layer {

    GameSprite * _player1;
    GameSprite * _player2;
    GameSprite * _ball;
    
    Vector<GameSprite*> _players;
    
    Label* _player1ScoreLabel;
    Label* _player2ScoreLabel;
    
    Size _screenSize;
    
    int _player1Score;
    int _player2Score;
    
    void playerScore( int player);
    
public:
    
    GameLayer();
    virtual ~GameLayer();
    virtual bool init();
    
    static Scene* scene();
    
    
    void onTouchesBegan(const std::vector<Touch*> &touches, Event* event);
    void onTouchesMoved(const std::vector<Touch*> &touches, Event* event);
    void onTouchesEnded(const std::vector<Touch*> &touches, Event* event);
    
    void update(float dt);
    CREATE_FUNC(GameLayer);
};




#endif /* defined(__AirKockey__GameLayer__) */
